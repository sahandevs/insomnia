// @flow
import * as React from 'react';
import autobind from 'autobind-decorator';
import { Dropdown, DropdownButton, DropdownDivider, DropdownItem } from '../base/dropdown';
import { showModal } from '../modals';
import * as syncStorage from '../../../sync/storage';
import * as session from '../../../sync/session';
import * as sync from '../../../sync';
const path = require('path');
import WorkspaceShareSettingsModal from '../modals/workspace-share-settings-modal';
import SetupSyncModal from '../modals/setup-sync-modal';
const exec = require('child_process').exec;
import { all } from '../../../models';
import type { Workspace } from '../../../models/workspace';
const { getCurrentWindow, globalShortcut } = require('electron').remote;

var reload = () => {
  getCurrentWindow().reload();
};

type Props = {
  workspace: Workspace,

  // Optional
  className?: string
};

type State = {
  loggedIn: boolean | null,
  loading: boolean,
  resourceGroupId: string | null,
  syncMode: string | null,
  syncPercent: number,
  workspaceName: string
};

@autobind
class SyncDropdown extends React.PureComponent<Props, State> {
  _hasPrompted: boolean;
  _isMounted: boolean;

  constructor(props: Props) {
    super(props);

    this._hasPrompted = false;
    this._isMounted = false;

    this.state = {
      loggedIn: null,
      loading: false,
      resourceGroupId: null,
      syncMode: null,
      syncPercent: 0,
      progress: 'Idle',
      workspaceName: ''
    };
  }

  _handleShowShareSettings() {
    showModal(WorkspaceShareSettingsModal, { workspace: this.props.workspace });
  }

  async _handleSyncResourceGroupId() {
    const { resourceGroupId } = this.state;

    // Set loading state
    this.setState({ loading: true });

    await sync.getOrCreateConfig(resourceGroupId);
    await sync.pull(resourceGroupId);
    await sync.push(resourceGroupId);

    await this._reloadData();

    // Unset loading state
    this.setState({ loading: false });
  }

  async _reloadData() {
    const loggedIn = session.isLoggedIn();

    if (loggedIn !== this.state.loggedIn) {
      this.setState({ loggedIn });
    }

    if (!loggedIn) {
      return;
    }

    // Get or create any related sync data
    const { workspace } = this.props;
    const { resourceGroupId } = await sync.getOrCreateResourceForDoc(workspace);
    const config = await sync.getOrCreateConfig(resourceGroupId);

    // Analyze it
    const dirty = await syncStorage.findDirtyResourcesForResourceGroup(resourceGroupId);
    const all = await syncStorage.findResourcesForResourceGroup(resourceGroupId);
    const numClean = all.length - dirty.length;
    const syncPercent = all.length === 0 ? 100 : parseInt((numClean / all.length) * 1000) / 10;

    if (this._isMounted) {
      this.setState({
        resourceGroupId,
        syncPercent,
        syncMode: config.syncMode,
        workspaceName: workspace.name
      });
    }
  }

  async _handleShowSyncModePrompt() {
    showModal(SetupSyncModal, {
      onSelectSyncMode: async syncMode => {
        await this._reloadData();
      }
    });
  }

  componentDidMount() {
    this._isMounted = true;
    syncStorage.onChange(this._reloadData);
    this._reloadData();
  }

  componentWillUnmount() {
    syncStorage.offChange(this._reloadData);
    this._isMounted = false;
  }

  componentDidUpdate() {
    const { resourceGroupId, syncMode } = this.state;

    if (!resourceGroupId) {
      return;
    }

    // Sync has not yet been configured for this workspace, so prompt the user to do so
    const isModeUnset = !syncMode || syncMode === syncStorage.SYNC_MODE_UNSET;
    if (isModeUnset && !this._hasPrompted) {
      this._hasPrompted = true;
      this._handleShowSyncModePrompt();
    }
  }

  _getSyncDescription(syncMode: string | null, syncPercentage: number) {
    let el = null;
    if (syncMode === syncStorage.SYNC_MODE_NEVER) {
      el = <span>Sync Disabled</span>;
    } else if (syncPercentage === 100) {
      el = <span>Sync Up To Date</span>;
    } else if (syncMode === syncStorage.SYNC_MODE_OFF) {
      el = (
        <span>
          <i className="fa fa-pause-circle-o" /> Sync Required
        </span>
      );
    } else if (syncMode === syncStorage.SYNC_MODE_ON) {
      el = <span>Sync Pending</span>;
    } else if (!syncMode || syncMode === syncStorage.SYNC_MODE_UNSET) {
      el = (
        <span>
          <i className="fa fa-exclamation-circle" /> Configure Sync
        </span>
      );
    }

    return el;
  }

  render() {
    const { className } = this.props;
    const { resourceGroupId, loading, loggedIn } = this.state;
    const { syncMode, progress } = this.state;
    return (
      <div className={className}>
        <Dropdown wide className="wide tall">
          <DropdownButton className="btn btn--compact wide">
            <span>
              <i className="fa fa-exclamation-circle" /> Sync
            </span>
          </DropdownButton>
          <DropdownDivider>
            Status {progress} {'   '}
            {this.state.progress !== 'Idle' && <i className="fa fa-refresh fa-spin" />}
          </DropdownDivider>

          <DropdownItem
            disabled={this.state.progress !== 'Idle'}
            onClick={() => {
              if (this.state.progress !== 'Idle') {
                alert(`Another Sync Progress is running (${this.state.progress})`);
                return;
              }
              this.onRenewClick();
            }}
            stayOpenAfterClick>
            <i className="fa fa-minus-circle" />
            Renew & Sync
          </DropdownItem>

          <DropdownItem
            disabled={this.state.progress !== 'Idle'}
            onClick={() => {
              if (this.state.progress !== 'Idle') {
                alert(`Another Sync Progress is running (${this.state.progress})`);
                return;
              }
              this.onPushClick();
            }}
            stayOpenAfterClick>
            <i className="fa fa-upload" />
            Push
          </DropdownItem>

          <DropdownItem
            disabled={this.state.progress !== 'Idle'}
            onClick={() => {
              if (this.state.progress !== 'Idle') {
                alert(`Another Sync Progress is running (${this.state.progress})`);
                return;
              }
              this.onSyncClick();
            }}
            stayOpenAfterClick>
            <i className="fa fa-cloud-upload" />
            Sync
          </DropdownItem>
        </Dropdown>
      </div>
    );
  }

  onSyncClick() {
    const { settings } = this.props;
    if (settings.syncPath == null || settings.syncPath == '') {
      alert('Please define the sync repository path in the settings');
      return;
    }
    let pathName = path.join(settings.syncPath, 'all.json');
    this.setState({ progress: 'Syncing' });
    require('simple-git')(this.props.settings.syncPath)
      .fetch({ origin: null, master: null })
      .raw(['reset', '--hard', 'origin/master'])
      .pull(error => {
        if (error) {
          alert(`Push Failed: \n ${error}`);
          this.setState({ progress: 'Idle' });
        } else {
          this.props.handleImportFileToWorkspaceAuto(pathName, this.props.workspaceId, () => {
            this.setState({ progress: 'Idle' });
          });
        }
      });
  }

  onRenewClick() {
    const { settings } = this.props;
    if (settings.syncPath == null || settings.syncPath == '') {
      alert('Please define the sync repository path in the settings');
      return;
    }
    let pathName = path.join(settings.syncPath, 'all.json');
    this.setState({ progress: 'Renewing' });
    require('simple-git')(this.props.settings.syncPath)
      .fetch({ origin: null, master: null })
      .raw(['reset', '--hard', 'origin/master'])
      .pull(error => {
        if (error) {
          alert(`Push Failed: \n ${error}`);
          this.setState({ progress: 'Idle' });
        } else {
          all().forEach(item => {
            if (
              item.name == 'Workspace' ||
              item.name == 'Environment' ||
              item.name == 'Request' ||
              item.name == 'Request Meta'
            ) {
              if (item.name == 'Workspace') {
                item.all().then(o1 => {
                  var counter = 0;
                  o1.forEach(ws => {
                    if (ws.name == 'Playground') {
                      counter += 1;
                    }
                  });
                  if (counter == 0) {
                    item.create({ name: 'Playground' });
                  }
                });
              }
              item.all().then(o => {
                o.forEach(workspace => {
                  if (workspace.name != 'Playground') {
                    item.remove(workspace);
                    item.remove(workspace._id);
                  }
                });
              });
            }
          });
          setTimeout(() => {
            this.props.handleImportFileToWorkspaceAuto(pathName, this.props.activeWorkspace, () => {
              this.setState({ progress: 'Idle' });
            });
          }, 500);
        }
      });
  }

  onPushClick() {
    const { settings } = this.props;
    if (settings.syncPath == null || settings.syncPath == '') {
      alert('Please define the sync repository path in the settings');
      return;
    }
    // alert(`${settings.syncPath}`/)
    let pathName = path.join(settings.syncPath, 'all.json');
    // alert(pathName)
    this.setState({ progress: 'Pushing' });
    this.props.handleExportFileAuto(pathName, () => {
      this.setState({ progress: 'Pushing' });
      require('simple-git')(this.props.settings.syncPath)
        .add('.')
        .commit('sync')
        .push('origin', 'master', { '--force': null }, error => {
          if (error) {
            alert(`Push Failed: \n ${error}`);
          }
          this.setState({ progress: 'Idle' });
        });
    });
  }

  git(command, cb = () => {}) {
    let pathToRepo = path.join(this.props.settings.syncPath, '.git');
    let cmd = `git --git-dir "${pathToRepo}" ${command}`;
    exec(cmd, (error, stdOut, srtErr) => {
      console.log(error, stdOut, srtErr);
      cb();
    });
  }
}

export default SyncDropdown;
